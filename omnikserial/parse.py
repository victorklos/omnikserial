#!/usr/bin/env python3

"""Parsing routines for Omnik serial protocol packets."""

from collections import namedtuple
from struct import unpack


Header = namedtuple('Header', 'src dest control func')

InverterInfo = namedtuple(
    'InverterInfo', 'num_phases rated_power model make serial')
YieldInfo = namedtuple(
    'YieldInfo',
    'temp vpv1 vpv2 ipv1 ipv2 iac_r iac_s iac_t vac_r vac_s vac_t '
    'fac pac_r pac_s pac_t e_today e_total h_total mode '
    'gv_fault gf_fault gz_fault tmp_fault pv_fault gfci_fault error')


# Constants
FUNC_QUERY_ID_INFO_REPLY = 0x83
FUNC_QUERY_INFO_REPLY = 0x90


def parse_packet(raw_packet):
    """Parse an incoming packet, returns a tuple with the header and payload.

       When possible, the payload is parsed and returned. If the payload is unknown of a parsing
       error occurs, the raw `bytearray` is returned."""
    header = Header._make(unpack('>HHBB', raw_packet.header))
    payload = None

    try:
        if header.func == FUNC_QUERY_ID_INFO_REPLY:
            fields = [s.decode().rsplit()[0] for s in
                      unpack('>c 2x 4s 9x 12s 16s 16s', raw_packet.data[:60])]
            fields[0] = int(fields[0])
            fields[1] = int(fields[1])
            payload = InverterInfo._make(fields)
        if header.func == FUNC_QUERY_INFO_REPLY:
            payload = YieldInfo._make(
                unpack('>3H 2x 2H 2x 10H 4x H 2i B 6H i', raw_packet.data[:65]))
    except TypeError:
        pass
    except ValueError:
        pass

    # if parsing failed or the function code is not implemented then return the raw data
    if payload is None:
        payload = raw_packet.data

    return (header, payload)

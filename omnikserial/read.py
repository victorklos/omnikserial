#!/usr/bin/env python3
# Single bytes are named `b`, no matter what `pylint` thinks:
# pylint: disable=invalid-name

"""Reading routines for Omnik serial protocol."""

from collections import namedtuple
from enum import Enum

from .parse import parse_packet


RawPacket = namedtuple('RawPacket', 'header data')


def read_packets(ser):
    """Generator that returns parsed packets read from `ser`."""
    reader = OmnikRawReader()

    for raw_packet in reader.read_raw_packets(ser):
        if raw_packet is not None:
            parsed_packet = parse_packet(raw_packet)
            if parsed_packet is not None:
                yield parsed_packet
        else:
            break


class OmnikRawReader():  # pylint: disable=too-few-public-methods
    """Stateful reader that can read a stream byte-for-byte, detect packets, verifies checksums
       and lets you profit."""

    class States(Enum):
        """Protocol states, see module documentation."""
        WAITING =  lambda self, c: self._read_waiting(c)   # noqa: E731, E272, E222
        MAGIC =    lambda self, c: self._read_magic(c)     # noqa: E731, E272, E222
        HEADER =   lambda self, c: self._read_header(c)    # noqa: E731, E272, E222
        DATA =     lambda self, c: self._read_data(c)      # noqa: E731, E272, E222
        CHECKSUM = lambda self, c: self._read_checksum(c)  # noqa: E731
        FINISH = 1

    def __init__(self):
        self.state = self.States.WAITING
        self.header = bytearray(7)
        self.data = bytearray(256)
        self.checksum = bytearray(2)
        self.datalen = 0
        self.index = 0

    def _read_waiting(self, b):
        """Handle a single byte in the `States.WAITING` state."""
        return self.States.MAGIC if b == 0x3a else self.States.WAITING

    def _read_magic(self, b):
        """Handle a single byte in the `States.MAGIC` state."""
        return self.States.HEADER if b == 0x3a else self.States.WAITING

    def _read_header(self, b):
        """Handle a single byte in the `States.HEADER` state."""
        self.header[self.index] = b
        if self.index == 6:
            self.datalen = b
            return self.States.DATA
        return self.States.HEADER

    def _read_data(self, b):
        """Handle a single byte in the `States.DATA` state."""
        self.data[self.index] = b
        if self.index == self.datalen-1:
            return self.States.CHECKSUM
        return self.States.DATA

    def _read_checksum(self, b):
        """Handle a single byte in the `States.CHECKUM` state."""
        self.checksum[self.index] = b
        if self.index == 1:
            return self.States.FINISH
        return self.States.CHECKSUM

    def _verify_checksum(self):
        """Calculates the checksum and compares it with the transmitted checksum. Returns True if
           they are equal, i.e. if the packet is valid."""
        checksum = sum(b'\x3a\x3a' + self.header + self.data[:self.datalen])
        data_checksum = self.checksum[0] * 256 + self.checksum[1]
        return (checksum % 0xffff) == data_checksum

    def read_raw_packets(self, ser):
        """Generator that yields `omnikserial.read.RawPacket`, one at a time. Returns
           when End-of-Stream (EOS) is reached."""
        while True:
            b = ser.read(1)
            if not b:
                return None

            old_state = self.state
            new_state = self.state(self, b[0])
            self.index = 0 if old_state != new_state else self.index+1

            if new_state != self.States.FINISH:
                self.state = new_state
            else:
                self.state = self.States.WAITING
                if self._verify_checksum():
                    yield RawPacket(self.header[:6], self.data[:self.datalen])

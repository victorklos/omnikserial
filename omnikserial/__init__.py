"""Reads and interprets packets from an Omnik solar converter."""
__version__ = '0.1.3'


from .read import read_packets  # noqa: F401

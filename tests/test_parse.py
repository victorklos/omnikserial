# pylint: disable=missing-docstring

"""Tests for `omnikserial.parse` module."""

from omnikserial.parse import Header, InverterInfo, YieldInfo, parse_packet


def test_can_parse_unknown_packet(load_packet):
    header, payload = parse_packet(load_packet('simple'))
    assert isinstance(header, Header)
    assert header.control == 17
    assert header.func == 0xc3
    assert len(payload) == 1


def test_can_parse_status_info(load_packet):
    header, payload = parse_packet(load_packet('status1'))
    assert isinstance(header, Header)
    assert isinstance(payload, YieldInfo)
    assert payload.h_total == 18222


def test_can_parse_inverter_info(load_packet):
    header, payload = parse_packet(load_packet('inv'))
    assert isinstance(header, Header)
    assert isinstance(payload, InverterInfo)
    assert payload.serial == "ABCD0123456S1900"
    assert payload.rated_power == 2000
    assert payload.num_phases == 1

# pylint: disable=missing-docstring

"""Helpers for tests."""

from io import BytesIO

import pytest
from omnikserial.read import OmnikRawReader


@pytest.fixture
def omnik_reader():
    return OmnikRawReader()


@pytest.fixture
def load_packet():
    def loader(which):
        reader = OmnikRawReader()
        stream = BytesIO(PACKETS[which])
        raw = next(reader.read_raw_packets(stream))
        return raw

    return loader


PACKETS = {
    'simple':   bytes.fromhex('3a 3a 00 01 01 00 11 c3 01 00 01 4b'),
    'badsum':   bytes.fromhex('3a 3a 00 01 01 00 11 c3 01 00 ff ff'),
    'status1':  bytes.fromhex('3a 3a 00 01 01 00 11 90 42 00 c5 06 5d 00 00 ff '
                              'ff 00 00 00 9e ff ff 00 01 ff ff ff ff 08 fb ff '
                              'ff ff ff 13 8e 00 00 ff ff ff ff ff ff ff ff 00 '
                              '31 00 01 45 26 00 00 47 2e 00 01 00 00 00 00 ff '
                              'ff 00 00 00 00 00 00 00 00 00 00 1b c1'),
    'status2':  bytes.fromhex('3a 3a 00 01 01 00 11 90 42 00 f2 09 b9 00 00 ff '
                              'ff 00 02 00 b4 ff ff 00 02 ff ff ff ff 09 13 ff '
                              'ff ff ff 13 8b 00 29 ff ff ff ff ff ff ff ff 00 '
                              '08 00 01 45 27 00 00 47 30 00 01 00 00 00 00 ff '
                              'ff 00 00 00 00 00 00 00 00 00 00 1b 7f'),
    'inv':      bytes.fromhex('3a 3a 00 01 01 00 11 83 4d 31 20 20 32 30 30 30 '
                              '4e 4c 31 86 e3 20 02 71 15 6f 6d 6e 69 6b 32 30 '
                              '30 30 74 6c 20 4f 6d 6e 69 6b 2d 53 6f 6c 61 72 '
                              '20 20 20 20 20 41 42 43 44 30 31 32 33 34 35 36 '
                              '53 31 39 30 30 33 36 30 30 20 20 56 20 20 30 30 '
                              '30 30 39 2d 30 34 14 c7'),
}

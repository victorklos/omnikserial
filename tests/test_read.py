# pylint: disable=missing-docstring,invalid-name,redefined-outer-name,protected-access,no-self-use

"""Tests for `omnikserial.read` module."""

import pytest


def test_reader_can_read_simple_message(load_packet):
    res = load_packet('simple')
    assert res.header == b'\x00\x01\x01\x00\x11\xc3'
    assert res.data == b'\x00'


def test_reader_verifies_checksum(load_packet):
    with pytest.raises(StopIteration):
        res = load_packet('badsum')
        assert res is None


class TestReaderStates():
    def test_default_state_is_waiting(self, omnik_reader):
        assert omnik_reader.state == omnik_reader.States.WAITING

    def test_waiting_state(self, omnik_reader):
        assert omnik_reader._read_waiting(0x00) == omnik_reader.States.WAITING
        assert omnik_reader._read_waiting(0x67) == omnik_reader.States.WAITING
        assert omnik_reader._read_waiting(0xFF) == omnik_reader.States.WAITING
        assert omnik_reader._read_waiting(0x3a) == omnik_reader.States.MAGIC

    def test_magic_state(self, omnik_reader):
        assert omnik_reader._read_magic(0x3a) == omnik_reader.States.HEADER

    def test_magic_state_fail(self, omnik_reader):
        assert omnik_reader._read_magic(0x00) == omnik_reader.States.WAITING

    def test_header_state(self, omnik_reader):
        datalen = 0x10
        assert omnik_reader._read_header(0x00) == omnik_reader.States.HEADER
        omnik_reader.index = 6
        assert omnik_reader._read_header(datalen) == omnik_reader.States.DATA
        assert omnik_reader.datalen == datalen

    def test_data_state(self, omnik_reader):
        omnik_reader.datalen = 2
        assert omnik_reader._read_data(0x00) == omnik_reader.States.DATA
        omnik_reader.index = 1
        assert omnik_reader._read_data(0x00) == omnik_reader.States.CHECKSUM

    def test_checksum_state(self, omnik_reader):
        assert omnik_reader._read_checksum(0x00) == omnik_reader.States.CHECKSUM
        omnik_reader.index = 1
        assert omnik_reader._read_checksum(0x00) == omnik_reader.States.FINISH

# Omnikserial

A protocol reader and parser for the Omniksol serial protocol.


## Usage

From your own python program, do something like:

```{python}
import serial
import omnikserial

ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

for _, payload in omnikserial.read_packets(ser):
    print(payload)
```

The API documentation is at <https://victorklos.gitlab.io/omnikserial/>.


## Development

Clone this project, then checkout the `Makefile` for details. This project uses
[poetry](https://github.com/sdispater/poetry).

If you are missing something, please open an issue. Pull requests are happily accepted.


## Details

### Protocol

Looks like a variant of MODBUS. Some implementation details were found
[here](https://github.com/IceRiverSmart/ComInverter/blob/master/src/Inverter.java).

### Documentation on serial ports on Raspberry

- <https://www.raspberrypi.org/documentation/configuration/uart.md>
- <https://raspberrypi.stackexchange.com/a/45571>
- <https://raspberrypi.stackexchange.com/questions/83610/gpio-pinout-orientation-raspberypi-zero-w>

Serial is on header pins 8 and 10. Don't forget to also connect GND (**but not Vcc!**)

### Omniksolar

- <https://www.domotiga.nl/boards/2/topics/4562> Thread with some details on the serial port
- <https://domotiga.nl/boards/2/topics/7051> Thread with photo of wifi module
- <https://domotiga.nl/projects/domotiga/wiki/Omniksol> The inverter in question


## Licence

MIT, see LICENCE file.

# Makefile for omnikserial

.PHONY: test build


test:
	poetry check
	poetry run flake8 --max-line-length=100
	poetry run pytest


build: test
	poetry build
